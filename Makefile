BUILD_DIR=build
SRC_DIR=src
CPP=clang++
CPP_FLAGS=-Ofast -march=native -mtune=native

.PHONY: all input clean always

all: input

input: $(BUILD_DIR)/input

$(BUILD_DIR)/input: always
	 $(CPP) $(CPP_FLAGS) -lncurses $(SRC_DIR)/input.cpp -o $(BUILD_DIR)/input

always:
	mkdir -p $(BUILD_DIR)

clean:
	rm -fr $(BUILD_DIR)/*
