#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <ncurses.h>
#include <string>

void save(std::fstream& file, std::fstream& buffer){
	buffer.swap(file);
}

int do_command(std::string commands, std::fstream &file, std::fstream &buffer){
	if (commands == "q"){
		return 1;
	} else if (commands == "wq") {
		save(file, buffer);
		return 1;
	} else if (commands == "w") {
		save(file, buffer);
		return 0;
	}
	return 2;
}

int command_handling(WINDOW * commandwin, std::fstream& file, std::fstream& buffer){
	int userInput;
	std::string commands;
	mvwprintw(commandwin, 1, 1, ":");
	wrefresh(commandwin);
GETCOMMAND: userInput = getch();
	if (userInput == 10) {
		switch (do_command(commands, file, buffer)){
			case 1:
				endwin();
				file.close();
				buffer.close();
			return 1;
			case 2:
				mvwprintw(commandwin, 1, 1, "%s is not a command", commands.c_str());
				wrefresh(commandwin);
		}
		return 0;
	} else if ('' == userInput) {
		wclear(commandwin);
		box(commandwin, 0, 0);
		wrefresh(commandwin);
		return 0;
	}
	commands += (char)userInput;
	mvwprintw(commandwin, 1, 2, "%s", commands.c_str());
	wrefresh(commandwin);
	goto GETCOMMAND;
}

std::string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (fgets(buffer, sizeof buffer, pipe) != NULL)
            result += buffer;
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

int main(int argc, char ** argv){

	int userInput;
	// NCURSES STARTS
	initscr();
	noecho();
	cbreak();

	// get screen size
	int yMax, xMax;
	getmaxyx(stdscr, yMax, xMax);

	// create a window for the input
	WINDOW * commandwin = newwin(3, xMax-12, yMax-5, 6);
	WINDOW * boxwin = newwin(yMax-8, xMax-12, 2, 6);
	WINDOW * filewin = newwin(yMax-10, xMax-14, 3, 7);
	refresh();

	std::string tmp = "basename ", tmp2 = "/tmp/vi_Dario48/";
	exec("mkdir -p /tmp/vi_Dario48");
	tmp += argv[1];
	tmp = exec(tmp.c_str());
	tmp.erase(std::remove_if(tmp.begin(), tmp.end(), isspace), tmp.end());
	tmp2 += tmp;
	std::fstream file{argv[1]}, buffer{tmp2};
	tmp.clear();
	tmp += "touch ";
	tmp += tmp2;
	exec(tmp.c_str());
	buffer << file.rdbuf();
	buffer.sync();
	tmp.clear();
	tmp += "fold -w ";
	tmp += std::to_string(xMax-14);
	tmp += " -s ";
	tmp += tmp2;
	exec(tmp.c_str());
	tmp.clear();
	tmp2.clear();


	box(commandwin, 0, 0);
	box(boxwin, 0, 0);
	wrefresh(boxwin);
	wrefresh(commandwin);
	
	std::string line;
	int y = 0;
	while(getline(buffer,line)){
		mvwprintw(filewin, y, 0, "%s\n", line.c_str());
		y++;
	}
	line.clear();
	wrefresh(filewin);
		
	do {
		userInput = getch();
		wclear(commandwin);
		box(commandwin, 0, 0);
		wrefresh(commandwin);
		if(':' == userInput){
			if(command_handling(commandwin, file, buffer)) return 0;
		}
		if ('i' == userInput){

		}
	} while (true);
}
